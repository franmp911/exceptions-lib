package com.fjmpaez.exceptionslib;

import org.springframework.http.HttpStatus;

public interface ErrorPrinter {

    HttpStatus getHttpStatus();

    String getErrorCode();

    String getDescription();

    String getExternalErrorCode();

}
