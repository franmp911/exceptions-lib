package com.fjmpaez.exceptionslib.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import brave.Tracer;

@Component
public class Utils {

    @Autowired
    private Tracer tracer;

    public String getCurrentCorrelationId() {
        return tracer.currentSpan().context().traceIdString();
    }
}
